int  product(int a, int b)
{

    while (a == 0 || b == 0){
       return 0;
    }
     if (b < 0){
        return - a + product(a, b+1);
    }
    else
    {
        return a + product(a, b-1);
    }
    
    //complete me (recursively)!
  
}
